︠6c78712d-9f41-4632-9f08-0329e9ae4742︠
# NumPy
︡3e80b55e-21ae-4de9-b504-32e28e051626︡
︠d19916c6-e508-47b5-a2cb-7c9bc500d745s︠
import numpy as np
x = [284, 965, 731, 956, 654, 389, 722, 131]
y = np.array(x)
︡26a6c532-a455-4d69-afcf-dbe0a57e7eeb︡{"done":true}︡
︠a799f4f7-a9b3-442d-ab91-0267f571dedfs︠
print type(x), x
print type(y), y
︡5b974af3-1bac-40ac-847b-1cb7ec2d747b︡{"stdout":"<type 'list'> [284, 965, 731, 956, 654, 389, 722, 131]\n"}︡{"stdout":"<type 'numpy.ndarray'> [284 965 731 956 654 389 722 131]\n"}︡{"done":true}︡
︠407743ae-0301-42b0-9086-fc673b37771f︠
# получение элементов по индексам:
# индексы заданы как интервал:
print x[1:3]
print y[1:3]
︡ac6cb3d1-460c-4783-8541-ad819eadec93︡
︠d36989cc-d502-4f30-acd3-3af7a2ff9476s︠
# индексы заданы через перечисление:
print x[[0,5,7]]
︡39e2f249-2cd0-46bd-bb38-a1dca46ebc4b︡{"stderr":"\n\n*** WARNING: Code contains non-ascii characters    ***\n\n\nError in lines 1-1\nTraceback (most recent call last):\n  File \"/cocalc/lib/python2.7/site-packages/smc_sagews/sage_server.py\", line 1013, in execute\n    exec compile(block+'\\n', '', 'single') in namespace, locals\n  File \"\", line 1, in <module>\nTypeError: list indices must be integers, not list\n"}︡{"done":true}︡
︠0201e095-3911-4da6-9a64-75d005f284ebs︠
print y[[0,5,7]]
︡959ba028-3c72-4ee6-a0d5-418cd94eafe3︡{"stdout":"[284 389 131]\n"}︡{"done":true}︡
︠1d1255fd-454c-4b7f-95bf-f0938aad92c2s︠
print y[[True, False,True,False,True, False,True,False]]
︡e10080f7-ad31-47ac-a341-81f9b644be96︡{"stdout":"[284 731 654 722]\n"}︡{"done":true}︡
︠42cae329-948d-491d-9016-dbedae590a8cs︠
print y[y>400]
︡19969fdd-5876-42b0-b058-40a6e488d1ec︡{"stdout":"[965 731 956 654 722]\n"}︡{"done":true}︡
︠4b50b1fb-f0d2-42e2-83a9-c9ed967c3929s︠
print x * 2
︡0dd14be3-7b07-4aed-9c15-6b06419b1c99︡{"stdout":"[284, 965, 731, 956, 654, 389, 722, 131, 284, 965, 731, 956, 654, 389, 722, 131]\n"}︡{"done":true}︡
︠7b5c5723-a69a-4f1b-a71b-6aae87b5bbd2s︠
print y * 2
︡feb4f6f4-18e7-47f0-b647-4bbd397fb7c6︡{"stdout":"[ 568 1930 1462 1912 1308  778 1444  262]\n"}︡{"done":true}︡
︠ac65bbe3-f5a0-40b3-bbe2-0926274fa598s︠
print x ** 2
︡cf64b170-1d84-4689-b3cd-7a3b5b05cbc3︡{"stderr":"Error in lines 1-1\nTraceback (most recent call last):\n  File \"/cocalc/lib/python2.7/site-packages/smc_sagews/sage_server.py\", line 1013, in execute\n    exec compile(block+'\\n', '', 'single') in namespace, locals\n  File \"\", line 1, in <module>\n  File \"sage/rings/integer.pyx\", line 2042, in sage.rings.integer.Integer.__pow__ (build/cythonized/sage/rings/integer.c:13839)\n    return self ** int(n)\nTypeError: unsupported operand type(s) for ** or pow(): 'list' and 'int'\n"}︡{"done":true}︡
︠2b9d8d37-e6a0-4256-827b-e4b31faa24d0s︠
print y ** 2
︡61e10a11-f157-4ab8-8c72-53de88552b05︡{"stdout":"[ 80656 931225 534361 913936 427716 151321 521284  17161]\n"}︡{"done":true}︡
︠a1e62e3c-2eef-411a-99e1-79ae269ac7c3s︠
matrix = [[232, 456, 841, 762, 393], [912, 491, 634, 902, 643], [812, 346, 351, 672, 213]]
np_matrix = np.array(matrix)
print matrix
print nd_matrix
︡de0e1d10-7d62-44df-878f-e82f1839d551︡{"stdout":"[[232, 456, 841, 762, 393], [912, 491, 634, 902, 643], [812, 346, 351, 672, 213]]\n"}︡{"stdout":"[[232 456 841 762 393]\n [912 491 634 902 643]\n [812 346 351 672 213]]\n"}︡{"done":true}︡
︠f3ef1821-c759-4919-8904-f29b4e73fe6fs︠
print matrix[2][1]
print np_matrix[2][1]
print np_matrix[2, 1]
︡137f2a6c-0925-48e8-b435-6415940b2bdb︡{"stdout":"346\n"}︡{"stdout":"346\n"}︡{"stdout":"346\n"}︡{"done":true}︡
︠56b6f024-b9e0-48d2-a1db-48dfb4e9d969︠

︡3bbd1585-249e-4fad-84ab-7d305fcb49dd︡
︠8a5b952d-9d1f-4e63-a1c2-b5a16af860f0s︠
print np.random.rand()
︡89c8b197-fff1-4fe2-8082-866056a0f6e3︡{"stdout":"0.0682049167791\n"}︡{"done":true}︡
︠3eaaa587-ef9e-44da-a4e1-ccf7b26cf8f3s︠
print np.random.randn()
︡2a816e80-ef23-43c4-bd89-f4b5a42aca25︡{"stdout":"0.0359341162705\n"}︡{"done":true}︡
︠3d9310df-16e5-4fb2-b4d3-d46d81e9ce64s︠
print np.random.randn(8,5)
︡561deb51-968f-4246-b4d3-cdc9d89391a4︡{"stdout":"[[ 0.5680316  -0.83362708  0.803399   -1.53448199 -0.54470775]\n [ 0.26908572 -0.44906721  2.90704741 -0.4025739   1.25479312]\n [-1.3685298   0.70263622 -0.05355485  1.44940874  0.01893506]\n [ 1.96737046 -0.90255751  0.17663947  0.90459131  0.07565919]\n [ 0.11231039 -0.4173762  -0.90277832  0.12912713  0.87929508]\n [ 1.93327009  0.02169193  0.59686114 -0.27802736 -0.49753301]\n [-0.35943416  0.92684447  1.4677375  -0.58054045  0.34473021]\n [-0.29458909  2.31910682  1.40984361 -0.1384277  -1.00995466]]\n"}︡{"done":true}︡
︠b624723e-59ce-45cb-ba4f-4592fad29dfds︠
print range(0,8,1)
︡fc24e48b-34ef-48d3-a31b-61ef5e761c67︡{"stdout":"[0, 1, 2, 3, 4, 5, 6, 7]\n"}︡{"done":true}︡
︠44b80e71-22b9-4cf2-855e-30f64b3c9b72s︠
print range(0, 8, 0.1)
︡52d5a501-b6a0-4da0-8b69-827cfdb17dd6︡{"stderr":"Error in lines 1-1\nTraceback (most recent call last):\n  File \"/cocalc/lib/python2.7/site-packages/smc_sagews/sage_server.py\", line 1013, in execute\n    exec compile(block+'\\n', '', 'single') in namespace, locals\n  File \"\", line 1, in <module>\nValueError: range() step argument must not be zero\n"}︡{"done":true}︡
︠9385b50c-2a02-43af-9b3f-217112269146s︠
print np.arange(0,8,0.1)
︡15241763-8653-4b29-bbe5-b20e758415e3︡{"stdout":"[ 0.   0.1  0.2  0.3  0.4  0.5  0.6  0.7  0.8  0.9  1.   1.1  1.2  1.3  1.4\n  1.5  1.6  1.7  1.8  1.9  2.   2.1  2.2  2.3  2.4  2.5  2.6  2.7  2.8  2.9\n  3.   3.1  3.2  3.3  3.4  3.5  3.6  3.7  3.8  3.9  4.   4.1  4.2  4.3  4.4\n  4.5  4.6  4.7  4.8  4.9  5.   5.1  5.2  5.3  5.4  5.5  5.6  5.7  5.8  5.9\n  6.   6.1  6.2  6.3  6.4  6.5  6.6  6.7  6.8  6.9  7.   7.1  7.2  7.3  7.4\n  7.5  7.6  7.7  7.8  7.9]\n"}︡{"done":true}︡
︠befe0d79-d0c8-4472-bdfd-1a7a0adb2878s︠
%timeit range(0,10000)
︡52fb4e7b-021a-4e8d-8084-064019c35c75︡{"stdout":"625 loops, best of 3: 108 µs per loop"}︡{"stdout":"\n"}︡{"done":true}︡
︠f3d4f213-94c1-4437-98e8-b77b6dbd6ebfs︠
%timeit np.arange(0,10000)
︡26ad1d9a-e371-4daf-be93-9752e89184fe︡{"stdout":"625 loops, best of 3: 21.9 µs per loop\n"}︡{"done":true}︡
︠ab4df42f-20d5-4011-b2f8-dc3f73a38768s︠
# SciPy
︡d2e3129b-a0cd-4a46-b021-a3aec5446829︡{"done":true}︡
︠01d8cc86-3a97-4faa-aec5-7d40b9cdbd79s︠
from scipy import optimize as opt
︡d1c927da-b953-44f8-a13c-0fed32ce0337︡{"done":true}︡
︠11c359c0-565d-48b8-85c9-36bd31e0823bs︠
def f(x):
    return (x[0] - 310)**2 + (x[1] + 452)**2 - 45

print f([310,-452])
︡ca848cf0-a66a-4cd4-b23e-136c9c383f89︡{"stdout":"-45\n"}︡{"done":true}︡
︠5e8fa598-b619-43ef-bc26-34a376d0d04es︠
x_min = opt.minimize(f, [-200,500])
print x_min
︡2cbff63a-d614-4c80-81c3-dbea7b86d16f︡{"stdout":"      fun: -44.99999999998896\n hess_inv: array([[ 0.50283637, -0.01271055],\n       [-0.01271055,  0.55546583]])\n      jac: array([ -1.90734863e-06,   6.67572021e-06])\n  message: 'Optimization terminated successfully.'\n     nfev: 44\n      nit: 6\n     njev: 11\n   status: 0\n  success: True\n        x: array([ 309.99999907, -451.99999681])\n"}︡{"done":true}︡
︠51e36cd2-6d28-4606-aa9b-4f44bd47676b︠
from scipy.optimize import rosen # функция Розенброка. Имеет минимум равный 0 в точках [1,1,...,1]
x0 = [1.3, 0.7, 0.8, 1.9, 1.2] # первоначальное предположение минимума
print rosen(x0)
res = opt.minimize(rosen, x0, method='Nelder-Mead', tol=1e-6)
print res
︡5601f775-d8da-48f3-b8b4-3b2775a06b4d︡{"stdout":"848.22\n"}︡{"stdout":" final_simplex: (array([[ 1.00000002,  1.00000002,  1.00000007,  1.00000015,  1.00000028],\n       [ 0.99999999,  0.99999996,  0.99999994,  0.99999986,  0.99999971],\n       [ 1.00000005,  1.00000007,  1.00000017,  1.00000031,  1.00000063],\n       [ 1.00000004,  1.00000008,  1.00000013,  1.00000025,  1.00000047],\n       [ 0.99999999,  0.99999996,  0.99999994,  0.99999984,  0.99999963],\n       [ 1.00000005,  1.00000004,  1.00000003,  1.00000003,  1.00000004]]), array([  1.94206402e-13,   2.44964782e-13,   3.10422870e-13,\n         3.37952410e-13,   5.52173609e-13,   7.16586838e-13]))\n           fun: 1.9420640199868412e-13\n       message: 'Optimization terminated successfully.'\n          nfev: 494\n           nit: 295\n        status: 0\n       success: True\n             x: array([ 1.00000002,  1.00000002,  1.00000007,  1.00000015,  1.00000028])\n"}︡{"done":true}︡
︠39498d88-8529-4b28-8351-5c898e5ac61cs︠
from scipy import linalg
a = np.array([[810, 578, 245], [620, 908, 615], [930, 238, 655]])
b = np.array([120, 140, 160])

x = linalg.solve(a, b)
print x
︡af652e8a-1b6e-42c2-982e-4c2341eb1733︡{"stdout":"[ 0.10491236  0.02386358  0.08664423]\n"}︡{"done":true}︡
︠8594cedd-47f5-41f1-bb7b-1cfada0abdd5s︠
print np.dot(a, x)
︡794cdd14-8218-433c-aab3-27fbb8c9f8f7︡{"stdout":"[ 120.  140.  160.]\n"}︡{"done":true}︡
︠5ce973ab-498b-4062-aa54-a4be29162dd8︠









