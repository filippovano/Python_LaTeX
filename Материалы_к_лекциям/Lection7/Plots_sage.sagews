︠27d9fd6a-b086-4167-9501-520187718bf5s︠
%var q, theta
plot(q * sin(q), (q, -2, 100))
︡51ff776d-2e6d-4497-83e7-fbe9fddcbf2d︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/1015/tmp_UkuQEf.svg","show":true,"text":null,"uuid":"b0f4bbdf-1654-4a9e-bca2-621b9f65b96a"},"once":false}︡{"done":true}︡
︠e963721b-607e-4957-bd6d-da75180d9712s︠
line([(0,0), (1,2), (1/2,pi), (1/2,pi/2)], color='darkgreen', thickness=3)
︡8dec880e-7006-44df-b7ad-7a4f3f35c0f9︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/496/tmp_2R6tGu.svg","show":true,"text":null,"uuid":"1b3e1a92-511a-482b-ad6f-195b10d8d0c4"},"once":false}︡{"done":true}︡
︠e0ef96c0-c6c1-40e4-8d5d-309c3981eee6s︠
parametric_plot([cos(x) + 2*cos(x/4), sin(x) - 2*sin(x/4)], (x,0,8*pi), color='green', thickness=3, fill = True)
︡d9127a81-b2f9-4bd4-9a12-84594e54fdaf︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/496/tmp_TjxbBd.svg","show":true,"text":null,"uuid":"4f79f99e-f148-4061-9453-e0b6373d5487"},"once":false}︡{"done":true}︡
︠d7be441e-d417-460f-b8a9-676156260e7fs︠
parametric_plot([16*sin(x)**3, 13*cos(x) - 5*cos(2*x) - 2*cos(3*x) - 1*cos(4*x)], (x,0,2*pi), color='red', thickness=3)
︡0c782f8f-a24c-45d4-b197-fa79985ee8bb︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/496/tmp_TeJBEP.svg","show":true,"text":null,"uuid":"16170712-78ab-4562-a2ce-6756be60829b"},"once":false}︡{"done":true}︡
︠6911e976-b304-4421-9d5b-b2c185480c5cs︠
show(points([(1,0), (sqrt(2)/2,sqrt(2)/2), (0,1), (1/2,1/2)], color='darkgreen', pointsize=50), aspect_ratio=1)
︡94e3e0b8-ebd7-4168-817a-6c8a07e21fc2︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/496/tmp__BggXp.svg","show":true,"text":null,"uuid":"dda56aab-54a7-457e-9d0d-052c87568e68"},"once":false}︡{"done":true}︡
︠00911a6d-836f-41cd-8538-285b33bdaae2s︠
a = polygon2d([(0,0), (1,2), (1/2,pi), (1/2,pi/2)], color='orange')
b = polygon2d([(0,0), (1,2), (1/2,pi), (1/2,pi/2)], color='black', fill=False, thickness=3)
show(a + b)
︡6cb0be53-eeaf-4db3-a0bf-74e8a5d6e65a︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/496/tmp_qL0EcM.svg","show":true,"text":null,"uuid":"fa7f3c5b-3c1f-4950-99a1-6f7f1a1a1343"},"once":false}︡{"done":true}︡
︠47887fe1-1ee0-41f9-916c-e94ea846e221s︠
︡ce46d91f-506e-4dc9-9958-430fb1e5fc2f︡
︠665adba4-e270-4bb7-b891-258daf9a79c0s︠
stats.TimeSeries(1000).randomize('normal').sums().plot()
︡23ae4e9b-4acf-43d2-88ec-689d93a43fbb︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/728/tmp_o5uwOh.svg","show":true,"text":null,"uuid":"8c214634-8f6e-4c5a-9295-8d759768dd82"},"once":false}︡{"done":true}︡
︠e7a3d148-4664-4fcf-a8b4-8878ebb8b092s︠
text(r"Text and LaTeX: $\alpha^3 + 1$", (1,1), color="black", fontsize=15, rotation=30)
︡89d3e786-995e-478d-86d5-2d6be55e80f4︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/496/tmp_ePzD2O.svg","show":true,"text":null,"uuid":"c351664c-fd45-4b0f-97bf-55036a4de570"},"once":false}︡{"done":true}︡
︠33ca2905-c7be-435e-be79-c163f1b51528s︠
show(cube(color=['red', 'blue', 'green'], frame_thickness=2,
         frame_color='brown', opacity=0.8), frame=False)
︡a8371e1c-f02a-41f4-aca7-0f841e2ca7be︡{"file":{"filename":"2c1cb12d-0cf0-4151-9654-879b9190ccf2.sage3d","uuid":"2c1cb12d-0cf0-4151-9654-879b9190ccf2"}}︡{"done":true}︡
︠4b610fea-e060-4f34-b1b8-f19c5cd7751ds︠
%var x y
plot3d(x * sin(y), (x, -5, 5), (y, -5, 5))
︡acc347b0-2428-4dad-b22d-e4bbeb052009︡{"file":{"filename":"9dda17c0-01d1-450d-8aab-2596252dbf97.sage3d","uuid":"9dda17c0-01d1-450d-8aab-2596252dbf97"}}︡{"done":true}︡
︠e6f8fe44-7eeb-4cf6-a796-188ec4371bcds︠
%var u
parametric_plot3d( (sin(u), cos(u), u/10), (u, 0, 20), thickness=5, color='green', plot_points=100)
︡25e31695-e322-4faf-a589-c99d889425b6︡{"file":{"filename":"2a00f191-11fc-4b2c-ac30-8c04c9df5b0d.sage3d","uuid":"2a00f191-11fc-4b2c-ac30-8c04c9df5b0d"}}︡{"done":true}︡
︠1c3a7f0c-10ac-4f61-88e8-351b30155e66s︠
%var u, v
fx = (3*(1+sin(v)) + 2*(1-cos(v)/2)*cos(u))*cos(v)
fy = (4+2*(1-cos(v)/2)*cos(u))*sin(v)
fz = -2*(1-cos(v)/2) * sin(u)
parametric_plot3d([fx, fy, fz], (u, 0, 2*pi), (v, 0, 2*pi), color="green", opacity=.7, mesh=1, spin=1)
︡f1ef315e-613c-4326-8c16-a8ad76d3ea8c︡{"file":{"filename":"8cc5c781-5bd0-434c-a80e-c9bce9dd8826.sage3d","uuid":"8cc5c781-5bd0-434c-a80e-c9bce9dd8826"}}︡{"done":true}︡
︠3b33d65b-01cd-4b66-b641-35118da7d7bcs︠
points = [(2,0,0), (0,2,0), (0,0,2), (-1,0,0), (0,-1,0), (0,0,-1)]
show(LatticePolytope(points).plot3d(), spin=5)
︡c7d317db-1cd4-4331-beec-d88fb6e61f44︡{"file":{"filename":"efffa300-88f6-408e-981c-c864f286a153.sage3d","uuid":"efffa300-88f6-408e-981c-c864f286a153"}}︡{"done":true}︡
︠adad8c48-142f-4219-a6e9-e8537d4e4ef9s︠
v = [(0,0,0)]
for i in range(1000):
    v.append([a+random()-.5 for a in v[-1]])
line3d(v, color='red', thickness=3, spin=3)
︡c94cfc30-23d5-4c7d-8e10-2af932b5d599︡{"file":{"filename":"a68414f1-0224-4bdc-a05c-113c6156df2b.sage3d","uuid":"a68414f1-0224-4bdc-a05c-113c6156df2b"}}︡{"done":true}︡
︠a9cb2282-a36c-4b4d-9c85-29e1e30ac343s︠
from sage.plot.plot3d.shapes import Torus
inner_radius = .3; outer_radius = 1
show(Torus(outer_radius, inner_radius, color='orange'), aspect_ratio=1, spin=3)
︡202f689c-c0ca-4256-b518-89ac5351c9fb︡{"file":{"filename":"4ed91af7-558e-4b0b-9fa9-9cbaf74984e9.sage3d","uuid":"4ed91af7-558e-4b0b-9fa9-9cbaf74984e9"}}︡︡{"done":true}
︠33b505fd-7a90-484e-b509-ac58ef92b73cs︠

plot(x * sin(x), (x, -2, 10))
print(type(sin(x)))
︡813590f5-965b-40f3-bbf8-e8528a519086︡{"file":{"filename":"/home/user/.sage/temp/project-ed5e768d-0132-4128-8179-c397a099b6cb/1015/tmp__jEgH6.svg","show":true,"text":null,"uuid":"fdd825f4-dffb-418a-b219-e5178c48c6ca"},"once":false}︡{"stdout":"<type 'sage.symbolic.expression.Expression'>\n"}︡{"done":true}︡
︠4d68016d-1654-485b-aabb-64150a06f1ef︠









