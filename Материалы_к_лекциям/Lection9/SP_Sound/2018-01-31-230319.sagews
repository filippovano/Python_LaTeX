︠a8f0dfab-3d0f-4d46-91ac-2d105f523fa3s︠
import numpy as np
import matplotlib.pyplot as plt
︡2690234e-2d7d-49b6-b343-0f047c640a15︡{"done":true}︡
︠57bab68e-4118-4ba2-a90f-54b8178817be︠
F0 = 262
A=2
phi = pi/4
fSampling = 131

t = np.linspace(-0.005,0.005,fSampling)

def y(x):
    return A * sin(2*pi*F0*x + phi)

l = []
for e in t:
    l.append((e, y(e)))

show(points(l, rgbcolor=(0.2,0.6, 0.1), pointsize=30) + plot(spline(l), -0.005, 0.005))
︡3d04a86c-f833-4fd3-b233-6c75a9938410︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/168/tmp_M5AMxL.svg","show":true,"text":null,"uuid":"a81144f9-523f-4be8-a178-8b28f2fcadac"},"once":false}︡{"done":true}︡
︠152863a6-a24c-4868-9cb3-70f89e2cadcc︠
F0 = 262
A=2
fSampling = 131

t = np.linspace(-0.005,0.005,fSampling)

def y(x):
    if 0 <= x < 1/F0:
        return 2*A*x*F0 - A
    elif x >= 1/F0:
        return y(x - 1/F0)
    elif x <= 0:
        return y(x + 1/F0)

l = []
for e in t:
    l.append((e, y(e)))

show(points(l, rgbcolor=(0.2,0.6, 0.1), pointsize=30) + plot(spline(l), -0.005, 0.005))
︡ec6ff816-cf5a-47bb-9896-9656a954d349︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/168/tmp_mnU1v4.svg","show":true,"text":null,"uuid":"1598db44-6547-4290-8081-4fcc56810f27"},"once":false}︡{"done":true}︡
︠c1c40a72-843b-4267-bb22-21a3ebc3a04e︠
F0 = 262
A = 2
fSampling = 131

t = np.linspace(-0.005,0.005,fSampling)

def y(x):
    if x == 0:
        return A
    else:
        return (A*sin(2*pi*x*F0)/(2*pi*x*F0)).n()

l = []
for e in t:
    l.append((e, y(e)))

show(points(l, rgbcolor=(0.2,0.6, 0.1), pointsize=30) + plot(spline(l), -0.005, 0.005))
︡73c1bfc3-3968-477a-873c-affcab176918︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/276/tmp_hRHkTK.svg","show":true,"text":null,"uuid":"25cae8b4-4ee5-4c7f-849e-984651151251"},"once":false}︡{"done":true}︡
︠f60e0ff0-f1b8-4304-a6b1-66540908b57e︠
x = np.random.rand(1000)
show(histogram(x, color=['green'], bins=50))
︡79aa0a8c-82ed-45a3-b1a8-e5d3fb4d266a︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/135/tmp_WpZavw.svg","show":true,"text":null,"uuid":"b6b62413-66fb-4d42-9ba2-b9d08dc45c00"},"once":false}︡{"done":true}︡
︠e29fe11e-29e5-4bfd-9ffe-ce3681137597︠
x = 0.447*np.random.randn(1000) - 2
show(histogram(x, color=['green'], bins=100))
︡aa8c99df-f78c-4567-89f5-c6036b279b46︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/171/tmp_pF8cpw.svg","show":true,"text":null,"uuid":"d731c5fd-3969-438f-9ccb-48e9a86b9a2a"},"once":false}︡{"done":true}︡
︠d767e6f4-2ee7-45ac-8f75-4cb7f31929cf︠
#!/usr/bin/python
# based on : www.daniweb.com/code/snippet263775.html
import math
import wave
import struct

# Audio will contain a long list of samples (i.e. floating point numbers describing the
# waveform).  If you were working with a very long sound you'd want to stream this to
# disk instead of buffering it all in memory list this.  But most sounds will fit in
# memory.
audio = []
#sample_rate = 44100.0
sample_rate = 8000.0


def append_silence(duration_milliseconds=500):
    """
    Adding silence is easy - we add zeros to the end of our array
    """
    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        audio.append(0.0)

    return


def append_sinewave(
        freq=440.0,
        duration_milliseconds=500,
        volume=1.0):
    """
    The sine wave generated here is the standard beep.  If you want something
    more aggresive you could try a square or saw tooth waveform.   Though there
    are some rather complicated issues with making high quality square and
    sawtooth waves... which we won't address here :)
    """

    #global audio # using global variables isn't cool.

    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        audio.append(volume * math.sin(2 * math.pi * freq * ( x / sample_rate )))

    return

def save_wav(file_name):
    # Open up a wav file
    wav_file = wave.open(file_name,"w")

    # wav params
    nchannels = int(1)

    sampwidth = int(2)

    # 44100 is the industry standard sample rate - CD quality.  If you need to
    # save on file size you can adjust it downwards. The stanard for low quality
    # is 8000 or 8kHz.
    print(len(audio))
    nframes = len(audio)
    comptype = "NONE"
    compname = "not compressed"
    wav_file.setparams((nchannels, sampwidth, sample_rate, nframes, comptype, compname))

    # WAV files here are using short, 16 bit, signed integers for the
    # sample size.  So we multiply the floating point data we have by 32767, the
    # maximum value for a short integer.  NOTE: It is theortically possible to
    # use the floating point -1.0 to 1.0 data directly in a WAV file but not
    # obvious how to do that using the wave module in python.
    for sample in audio:
        wav_file.writeframes(struct.pack('h', int( sample * 32767.0 )))

    wav_file.close()

    print("Music successful create")

    return

append_silence()
append_sinewave(freq = 524.0, volume=0.5)
append_sinewave(freq = 588.0, volume=0.5)
append_sinewave(freq = 660.0, volume=0.5)
append_sinewave(freq = 660.0, volume=0.5)
append_sinewave(freq = 698.0, volume=0.5)
append_sinewave(freq = 784.0, volume=0.5)
append_sinewave(freq = 784.0, volume=0.5)
append_sinewave(freq = 880.0, volume=0.5)
append_sinewave(freq = 784.0, volume=0.5)
append_sinewave(freq = 698.0, volume=0.5)
append_sinewave(freq = 660.0, volume=0.5)
append_sinewave(freq = 588.0, volume=0.5)
append_sinewave(freq = 524.0, volume=0.5)
append_silence()

save_wav("output.wav")
︡6640df1b-728c-4219-9cf3-e85593cb1c6e︡{"stdout":"60000\nMusic successful create"}︡{"stdout":"\n"}︡{"done":true}︡
︠4c9806ed-0e9a-4b88-896d-ec1434b8ee54︠
q = [1, 2, 3, 4]
y = [1, 4, 9, 16]
k = np.polyfit(q, y, 2) # The 2 signifies a polynomial of degree 2
︡6b335d40-8bb5-47d2-90c6-f549338f7a49︡{"done":true}︡
︠877cf012-c609-4b2a-9744-a01f665a6714s︠
def f(i):
    return k[0]*i*i + k[1]*i + k[2]
︡c9f9e47b-9909-4844-9802-3237cbd68e6f︡{"done":true}︡
︠b5cefe5c-c0bd-4241-9fbd-3462bc9272b5s︠
plot(f(x), (x, -10, 10))
︡0c45f5f7-2f3f-4bcd-ad2f-45c296e38111︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/892/tmp_SBFCPw.svg","show":true,"text":null,"uuid":"66ead4ee-27b0-445e-8173-80e5b96b81e3"},"once":false}︡{"done":true}︡
︠8227c742-9670-4ec8-b8ca-b19682b5acads︠
from scipy.misc import derivative
    
l = []
for e in q:
    l.append((e, derivative(f, e, dx=1e-6)))
    
show(plot(spline(l), 0, 4))
︡7d1647c2-6380-432d-949d-58c2cb122329︡{"stdout":"verbose 0 (3749: plot.py, generate_plot_points) WARNING: When plotting, failed to evaluate function at 50 points.\n"}︡{"stdout":"verbose 0 (3749: plot.py, generate_plot_points) Last error message: ''\n"}︡{"file":{"filename":"/home/user/.sage/temp/project-d26c90c2-6398-4a18-9eff-32aaf390bae1/892/tmp_ebDIoy.svg","show":true,"text":null,"uuid":"c01359b9-d2cb-4e3a-b533-ea96a06a6c05"},"once":false}︡{"done":true}︡
︠c6f1511a-f455-4a12-a9b9-78162dc81a16︠
x = np.random.rand(1000)









